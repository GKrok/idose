import Vue from 'vue';
// import moment from 'moment';

export default {
    namespaced: true,
    state: {
        dbf: null,
        persons: {},
        currentPerson: {
            id: -1,
            name: '',
            surname: '',
            birthday: '2000-01-01',
            sex: 'M',
            pregnant: false,
            fetAge: 0,
            phone: '',
            // intakePath: 'Inhal',
            measurements: [],
            intakeShape: [],
        },
        currentMeasurement: {
            id: -1,
            pers_id: -1,
            meas_date: new Date(), 
            lat: 0, 
            lon: 0,
            meas_RN: 'I131',
            meas_val: 0, 
            meas_err: 0, 
            meas_MDA: 0
        },
        currentPhotoName: '',
        defIS: []
    },
    getters: {
        persons(state) {
            return state.persons;
        },
        currentPerson(state) {
            return state.currentPerson;
        },
        currentMeasurement(state) {
            return state.currentMeasurement;
        },
        currentPhotoName(state) {
            return state.currentPhotoName;
        },
        defaultIS(state) {
            return state.defIS;
        }
    },
    mutations: {
        setDB(state, dbObj) {
            state.dbf = dbObj;
        },
        clearCurrentPerson(state) {
            state.currentPerson.id = -1;
            state.currentPhotoName = '';
        },
        clearCurrentMeasurement(state) {
            state.currentMeasurement.id = -1;
        },
        fillCurrentMeasurement(state, rec) {
            Object.assign(state.currentMeasurement, rec);
        },
        // closeDB(state) {
        //     console.log('Closing DB');
        //     state.dbf.close();
        // },
        fillCurrentPerson(state, rec) {
            // Object.assign(state.currentPerson, rec);
            state.currentPerson.id = rec.id; 
            state.currentPerson.name = rec.name;
            state.currentPerson.surname = rec.surname;
            state.currentPerson.birthday = rec.birthday;
            state.currentPerson.sex = rec.sex;
            state.currentPerson.pregnant = rec.pregnant;
            state.currentPerson.fetAge = rec.fetAge;
            state.currentPerson.phone = rec.phone;

            state.currentPhotoName = 'id_' + state.currentPerson.id + '.jpg';
        },
        fillCurrentPersonMeasurements(state, meas) {
            state.currentPerson.measurements.splice(0, state.currentPerson.measurements.length);
            for(let i=0;i<meas.length;i++) {
                state.currentPerson.measurements.push(meas.item(i));
            }
        },
        fillCurrentPersonIntakeShapes(state, intSh) {
            state.currentPerson.intakeShape.splice(0, state.currentPerson.intakeShape.length);
            for(let i=0;i<intSh.length;i++) {
                state.currentPerson.intakeShape.push(intSh.item(i));
            }
        },
        setName(state, val) {
            state.currentPerson.name = val;
        },
        setSurname(state, val) {
            state.currentPerson.surname = val;
        },
        setPhone(state, val) {
            state.currentPerson.phone = val;
        },
        setBirthday(state, val) {
            state.currentPerson.birthday = val;
        },
        setSex(state, val) {
            state.currentPerson.sex = val;
            if(val==='M') {
                state.currentPerson.pregnant = false;
                state.currentPerson.fetAge = 0;
            }
        },
        setFetAge(state, val) {
            state.currentPerson.fetAge = val;
        },
        setPregnant(state, val) {
            state.currentPerson.pregnant = val ? 1 : 0; 
            if(!val) {state.currentPerson.fetAge=0};
        },
        addPerson(state, rec) {
            Vue.set(state.persons, rec.id, rec);
            state.currentPerson.id=rec.id;
        },
        setPerson(state, rec) {
            Object.assign(state.persons[rec.id], rec);
        },
        setCurrentPhotoName(state, name) {
            state.currentPhotoName = name;
        },
        deletePerson(state, key) {
            Vue.delete(state.persons, key);
        },
        clearPersons(state) {
            state.persons = {};
        },
        // setMeasurement(state, rec) {
        //     for(let i=0;i<state.currentPerson.measurements.length;i++) {
        //         if (state.currentPerson.measurements[i].id===rec.id) {
        //             Object.assign(state.currentPerson.measurements[i], rec);
        //         }    
        //     }
        // },
        deleteMeasurement(state, key) {
            for(let i=0;i<state.currentPerson.measurements.length;i++) {
                if (state.currentPerson.measurements[i].id===key) {
                    state.currentPerson.measurements.splice(i, 1);
                    break;
                }
            }    
        },
        deleteISPoint(state, key) {
            for(let i=0;i<state.currentPerson.intakeShape.length;i++) {
                if (state.currentPerson.intakeShape[i].id===key) {
                    state.currentPerson.intakeShape.splice(i, 1);
                    break;
                }
            }    
        },
        fillDefIS(state, recs) {
            state.defIS.splice(0, state.defIS.length);
            for(let i=0;i<recs.length;i++) {
                state.defIS.push(recs.item(i));
            }
        }
    },
    actions: {
        fillDBDemo({state, dispatch}) {
            state.dbf.sqlBatch([
                'DROP TABLE IF EXISTS Measurements;',
                'DROP TABLE IF EXISTS IntakeShapes;',
                'DROP TABLE IF EXISTS Persons;',
                'DROP TABLE IF EXISTS defIntakeShape;',
                'DROP TABLE IF EXISTS defPrm;',
                'CREATE TABLE IF NOT EXISTS Persons (id integer primary key, name, surname, birthday, sex TEXT DEFAULT "M", phone, pregnant NUMERIC DEFAULT 0, '+
                'fetAge INTEGER DEFAULT 0, intakePath TEXT DEFAULT "Inhal", useI132 NUMERIC DEFAULT 0, useFetus NUMERIC DEFAULT 0, calculated NUMERIC DEFAULT 0, ' + 
                'I131MOPScaleCoef NUMERIC DEFAULT 0, I131MOPDose NUMERIC DEFAULT 0, I131FetusScaleCoef NUMERIC DEFAULT 0, I131FetusDose NUMERIC DEFAULT 0, ' +
                'I132MOPScaleCoef NUMERIC DEFAULT 0, I132MOPDose NUMERIC DEFAULT 0, I132FetusScaleCoef NUMERIC DEFAULT 0, I132FetusDose NUMERIC DEFAULT 0);',
                'CREATE TABLE IF NOT EXISTS Measurements (id integer primary key, pers_id NOT NULL, meas_date, lat, lon, ' +
                'meas_RN, meas_val, meas_err, meas_MDA, ' + 
                'FOREIGN KEY(pers_id) REFERENCES Persons(id) ON DELETE CASCADE);',
                'CREATE INDEX IF NOT EXISTS measpers ON Measurements(pers_id)',
                'CREATE UNIQUE INDEX IF NOT EXISTS measind ON Measurements(pers_id, meas_date, meas_RN)',
                'CREATE TABLE IF NOT EXISTS IntakeShapes (id integer primary key, pers_id NOT NULL, int_date, int_RN, int_val, FOREIGN KEY(pers_id) REFERENCES Persons(id) ON DELETE CASCADE);',
                'CREATE INDEX IF NOT EXISTS intpers ON IntakeShapes(pers_id)',
                'CREATE UNIQUE INDEX IF NOT EXISTS intind ON IntakeShapes(pers_id, int_RN, int_date)',
                'CREATE TABLE IF NOT EXISTS defIntakeShape (id integer primary key, int_date, int_RN, int_val);',
                'CREATE UNIQUE INDEX IF NOT EXISTS defintind ON defIntakeShape(int_RN, int_date);',
                'CREATE TABLE IF NOT EXISTS defPrm (id integer primary key, dtAccident);',
                [ 'INSERT INTO Persons (name, surname, birthday, sex, phone) VALUES (?,?,?,?,?)', ['Person', 'One', '1969-05-21', 'M', '0672920265'] ],
                [ 'INSERT INTO Persons (name, surname, birthday, sex, phone) VALUES (?,?,?,?,?)', ['Person', 'Two', '1969-06-20', 'M', '0672920265'] ],
                [ 'INSERT INTO Persons (name, surname, birthday, sex, phone) VALUES (?,?,?,?,?)', ['Person', 'Three', '2009-05-13', 'F', '0672920265'] ],
                [ 'INSERT INTO Measurements (pers_id, meas_date, lat, lon, meas_RN, meas_val, meas_err, meas_MDA) VALUES (?,?,?,?,?,?,?,?)', [1, '2019-01-03T00:00', 0.0, 0.0, 'I131', 2, 10, 2.5]],
                [ 'INSERT INTO Measurements (pers_id, meas_date, lat, lon, meas_RN, meas_val, meas_err, meas_MDA) VALUES (?,?,?,?,?,?,?,?)', [1, '2019-01-05T00:00', 0.0, 0.0, 'I131', 2.5, 10, 2.7]],
                [ 'INSERT INTO Measurements (pers_id, meas_date, lat, lon, meas_RN, meas_val, meas_err, meas_MDA) VALUES (?,?,?,?,?,?,?,?)', [1, '2019-01-06T00:00', 0.0, 0.0, 'I131', 2, 10, 3]],
                [ 'INSERT INTO Measurements (pers_id, meas_date, lat, lon, meas_RN, meas_val, meas_err, meas_MDA) VALUES (?,?,?,?,?,?,?,?)', [2, '2019-01-03T00:00', 0.0, 0.0, 'I131', 2000, 20, 2.5]],
                [ 'INSERT INTO Measurements (pers_id, meas_date, lat, lon, meas_RN, meas_val, meas_err, meas_MDA) VALUES (?,?,?,?,?,?,?,?)', [2, '2019-01-05T00:00', 0.0, 0.0, 'I131', 2500, 15, 2.7]],
                [ 'INSERT INTO Measurements (pers_id, meas_date, lat, lon, meas_RN, meas_val, meas_err, meas_MDA) VALUES (?,?,?,?,?,?,?,?)', [2, '2019-01-06T00:00', 0.0, 0.0, 'I131', 2000, 20, 3]],
                [ 'INSERT INTO Measurements (pers_id, meas_date, lat, lon, meas_RN, meas_val, meas_err, meas_MDA) VALUES (?,?,?,?,?,?,?,?)', [2, '2019-01-02T03:00', 0.0, 0.0, 'I132', 1000, 10, 11]],
                [ 'INSERT INTO Measurements (pers_id, meas_date, lat, lon, meas_RN, meas_val, meas_err, meas_MDA) VALUES (?,?,?,?,?,?,?,?)', [2, '2019-01-03T00:00', 0.0, 0.0, 'I132', 2000, 10, 1]],
                [ 'INSERT INTO Measurements (pers_id, meas_date, lat, lon, meas_RN, meas_val, meas_err, meas_MDA) VALUES (?,?,?,?,?,?,?,?)', [2, '2019-01-04T00:00', 0.0, 0.0, 'I132', 2000, 10, 1]],
                [ 'INSERT INTO Measurements (pers_id, meas_date, lat, lon, meas_RN, meas_val, meas_err, meas_MDA) VALUES (?,?,?,?,?,?,?,?)', [2, '2019-01-05T00:00', 0.0, 0.0, 'I132', 1200, 15, 1]],
                [ 'INSERT INTO Measurements (pers_id, meas_date, lat, lon, meas_RN, meas_val, meas_err, meas_MDA) VALUES (?,?,?,?,?,?,?,?)', [3, '2019-01-03T00:00', 0.0, 0.0, 'I131', 2, 10, 2.5]],
                [ 'INSERT INTO Measurements (pers_id, meas_date, lat, lon, meas_RN, meas_val, meas_err, meas_MDA) VALUES (?,?,?,?,?,?,?,?)', [3, '2019-01-05T00:00', 0.0, 0.0, 'I131', 2.5, 10, 2.7]],
                [ 'INSERT INTO Measurements (pers_id, meas_date, lat, lon, meas_RN, meas_val, meas_err, meas_MDA) VALUES (?,?,?,?,?,?,?,?)', [3, '2019-01-06T00:00', 0.0, 0.0, 'I131', 2, 10, 3]],
                [ 'INSERT INTO defIntakeShape (int_date, int_RN, int_val) VALUES (?,?,?)', ['2019-01-01T00:05', 'I131', 2.5]],
                [ 'INSERT INTO defIntakeShape (int_date, int_RN, int_val) VALUES (?,?,?)', ['2019-01-02T00:05', 'I131', 5]],
                [ 'INSERT INTO defIntakeShape (int_date, int_RN, int_val) VALUES (?,?,?)', ['2019-01-03T00:05', 'I131', 1]],
                [ 'INSERT INTO IntakeShapes (pers_id, int_date, int_RN, int_val) VALUES (?,?,?,?)', [1, '2019-01-01T03:01', 'I131', 0]],
                [ 'INSERT INTO IntakeShapes (pers_id, int_date, int_RN, int_val) VALUES (?,?,?,?)', [1, '2019-01-01T12:05', 'I131', 0.1]],
                [ 'INSERT INTO IntakeShapes (pers_id, int_date, int_RN, int_val) VALUES (?,?,?,?)', [1, '2019-01-02T00:05', 'I131', 3]],
                [ 'INSERT INTO IntakeShapes (pers_id, int_date, int_RN, int_val) VALUES (?,?,?,?)', [1, '2019-01-02T19:12', 'I131', 0.5]],
                [ 'INSERT INTO IntakeShapes (pers_id, int_date, int_RN, int_val) VALUES (?,?,?,?)', [1, '2019-01-03T12:00', 'I131', 1]],
                [ 'INSERT INTO IntakeShapes (pers_id, int_date, int_RN, int_val) VALUES (?,?,?,?)', [1, '2019-01-04T00:00', 'I131', 0]],
                [ 'INSERT INTO IntakeShapes (pers_id, int_date, int_RN, int_val) VALUES (?,?,?,?)', [2, '2019-01-01T03:01', 'I131', 0]],
                [ 'INSERT INTO IntakeShapes (pers_id, int_date, int_RN, int_val) VALUES (?,?,?,?)', [2, '2019-01-01T12:05', 'I131', 0.1]],
                [ 'INSERT INTO IntakeShapes (pers_id, int_date, int_RN, int_val) VALUES (?,?,?,?)', [2, '2019-01-02T00:05', 'I131', 3]],
                [ 'INSERT INTO IntakeShapes (pers_id, int_date, int_RN, int_val) VALUES (?,?,?,?)', [2, '2019-01-02T19:12', 'I131', 0.5]],
                [ 'INSERT INTO IntakeShapes (pers_id, int_date, int_RN, int_val) VALUES (?,?,?,?)', [2, '2019-01-03T12:00', 'I131', 1]],
                [ 'INSERT INTO IntakeShapes (pers_id, int_date, int_RN, int_val) VALUES (?,?,?,?)', [2, '2019-01-04T00:00', 'I131', 0]],
                [ 'INSERT INTO IntakeShapes (pers_id, int_date, int_RN, int_val) VALUES (?,?,?,?)', [2, '2019-01-01T03:01', 'I132', 0]],
                [ 'INSERT INTO IntakeShapes (pers_id, int_date, int_RN, int_val) VALUES (?,?,?,?)', [2, '2019-01-01T12:05', 'I132', 0.1]],
                [ 'INSERT INTO IntakeShapes (pers_id, int_date, int_RN, int_val) VALUES (?,?,?,?)', [2, '2019-01-02T00:05', 'I132', 3]],
                [ 'INSERT INTO IntakeShapes (pers_id, int_date, int_RN, int_val) VALUES (?,?,?,?)', [2, '2019-01-02T19:12', 'I132', 0.5]],
                [ 'INSERT INTO IntakeShapes (pers_id, int_date, int_RN, int_val) VALUES (?,?,?,?)', [2, '2019-01-03T12:00', 'I132', 1]],
                [ 'INSERT INTO IntakeShapes (pers_id, int_date, int_RN, int_val) VALUES (?,?,?,?)', [2, '2019-01-04T00:00', 'I132', 0]],
                [ 'INSERT INTO IntakeShapes (pers_id, int_date, int_RN, int_val) VALUES (?,?,?,?)', [3, '2019-01-01T03:01', 'I131', 1]],
                [ 'INSERT INTO defPrm (id, dtAccident) VALUES (?,?)', [1, '2019-01-01T00:00']]
            ], function() {
                    dispatch('fillPersons');
                    // console.log('Before read def');
                    dispatch('getDefPrm');
                }, function(error) {
                    console.error('SQL batch ERROR: ' + error.message);
                }
            );
        },
        populateDB({commit, dispatch}) {
            window.sqlitePlugin.openDatabase({
                name: 'idose.db',
                location: 'default'
            }, function(dbf){
                commit('setDB', dbf);
                dbf.executeSql('PRAGMA foreign_keys = true');
                dbf.sqlBatch([
                    // 'DROP TABLE IF EXISTS Measurements;',
                    // 'DROP TABLE IF EXISTS IntakeShapes;',
                    // 'DROP TABLE IF EXISTS Persons;',
                    // 'DROP TABLE IF EXISTS defIntakeShape;',
                    // 'DROP TABLE IF EXISTS defPrm;',
                    'CREATE TABLE IF NOT EXISTS Persons (id integer primary key, name, surname, birthday, sex TEXT DEFAULT "M", phone, pregnant NUMERIC DEFAULT 0, '+
                    'fetAge INTEGER DEFAULT 0, intakePath TEXT DEFAULT "Inhal", useI132 NUMERIC DEFAULT 0, useFetus NUMERIC DEFAULT 0, calculated NUMERIC DEFAULT 0, ' + 
                    'I131MOPScaleCoef NUMERIC DEFAULT 0, I131MOPDose NUMERIC DEFAULT 0, I131FetusScaleCoef NUMERIC DEFAULT 0, I131FetusDose NUMERIC DEFAULT 0, ' +
                    'I132MOPScaleCoef NUMERIC DEFAULT 0, I132MOPDose NUMERIC DEFAULT 0, I132FetusScaleCoef NUMERIC DEFAULT 0, I132FetusDose NUMERIC DEFAULT 0);',
                    'CREATE TABLE IF NOT EXISTS Measurements (id integer primary key, pers_id NOT NULL, meas_date, lat, lon, ' +
                    'meas_RN, meas_val, meas_err, meas_MDA, ' + 
                    'FOREIGN KEY(pers_id) REFERENCES Persons(id) ON DELETE CASCADE);',
                    'CREATE INDEX IF NOT EXISTS measpers ON Measurements(pers_id)',
                    'CREATE UNIQUE INDEX IF NOT EXISTS measind ON Measurements(pers_id, meas_date, meas_RN)',
                    'CREATE TABLE IF NOT EXISTS IntakeShapes (id integer primary key, pers_id NOT NULL, int_date, int_RN, int_val, FOREIGN KEY(pers_id) REFERENCES Persons(id) ON DELETE CASCADE);',
                    'CREATE INDEX IF NOT EXISTS intpers ON IntakeShapes(pers_id)',
                    'CREATE UNIQUE INDEX IF NOT EXISTS intind ON IntakeShapes(pers_id, int_RN, int_date)',
                    'CREATE TABLE IF NOT EXISTS defIntakeShape (id integer primary key, int_date, int_RN, int_val);',
                    'CREATE UNIQUE INDEX IF NOT EXISTS defintind ON defIntakeShape(int_RN, int_date);',
                    'CREATE TABLE IF NOT EXISTS defPrm (id integer primary key, dtAccident);'
                    // [ 'INSERT INTO Persons (name, surname, birthday, sex, phone) VALUES (?,?,?,?,?)', ['Person', 'One', '1969-05-21', 'M', '0672920265'] ],
                    // [ 'INSERT INTO Persons (name, surname, birthday, sex, phone) VALUES (?,?,?,?,?)', ['Person', 'Two', '1969-06-20', 'M', '0672920265'] ],
                    // [ 'INSERT INTO Persons (name, surname, birthday, sex, phone) VALUES (?,?,?,?,?)', ['Person', 'Three', '2009-05-13', 'F', '0672920265'] ],
                    // [ 'INSERT INTO Measurements (pers_id, meas_date, lat, lon, meas_RN, meas_val, meas_err, meas_MDA) VALUES (?,?,?,?,?,?,?,?)', [1, '2019-01-03T00:00', 1.5, 1.5, 'I131', 2, 10, 2.5]],
                    // [ 'INSERT INTO Measurements (pers_id, meas_date, lat, lon, meas_RN, meas_val, meas_err, meas_MDA) VALUES (?,?,?,?,?,?,?,?)', [1, '2019-01-05T00:00', 1.5, 1.5, 'I131', 2.5, 10, 2.7]],
                    // [ 'INSERT INTO Measurements (pers_id, meas_date, lat, lon, meas_RN, meas_val, meas_err, meas_MDA) VALUES (?,?,?,?,?,?,?,?)', [1, '2019-01-06T00:00', 1.5, 1.5, 'I131', 2, 10, 3]],
                    // [ 'INSERT INTO Measurements (pers_id, meas_date, lat, lon, meas_RN, meas_val, meas_err, meas_MDA) VALUES (?,?,?,?,?,?,?,?)', [2, '2019-01-03T00:00', 1.5, 1.5, 'I131', 2000, 20, 2.5]],
                    // [ 'INSERT INTO Measurements (pers_id, meas_date, lat, lon, meas_RN, meas_val, meas_err, meas_MDA) VALUES (?,?,?,?,?,?,?,?)', [2, '2019-01-05T00:00', 1.5, 1.5, 'I131', 2500, 15, 2.7]],
                    // [ 'INSERT INTO Measurements (pers_id, meas_date, lat, lon, meas_RN, meas_val, meas_err, meas_MDA) VALUES (?,?,?,?,?,?,?,?)', [2, '2019-01-06T00:00', 1.5, 1.5, 'I131', 2000, 20, 3]],
                    // [ 'INSERT INTO Measurements (pers_id, meas_date, lat, lon, meas_RN, meas_val, meas_err, meas_MDA) VALUES (?,?,?,?,?,?,?,?)', [2, '2019-01-02T00:00', 1.5, 1.5, 'I132', 1000, 10, 11]],
                    // [ 'INSERT INTO Measurements (pers_id, meas_date, lat, lon, meas_RN, meas_val, meas_err, meas_MDA) VALUES (?,?,?,?,?,?,?,?)', [2, '2019-01-03T00:00', 1.5, 1.5, 'I132', 2000, 10, 1]],
                    // [ 'INSERT INTO Measurements (pers_id, meas_date, lat, lon, meas_RN, meas_val, meas_err, meas_MDA) VALUES (?,?,?,?,?,?,?,?)', [2, '2019-01-04T00:00', 1.5, 1.5, 'I132', 3000, 10, 1]],
                    // [ 'INSERT INTO Measurements (pers_id, meas_date, lat, lon, meas_RN, meas_val, meas_err, meas_MDA) VALUES (?,?,?,?,?,?,?,?)', [2, '2019-01-05T00:00', 1.5, 1.5, 'I132', 1000, 10, 1]],
                    // [ 'INSERT INTO Measurements (pers_id, meas_date, lat, lon, meas_RN, meas_val, meas_err, meas_MDA) VALUES (?,?,?,?,?,?,?,?)', [3, '2019-01-03T00:00', 1.5, 1.5, 'I131', 2, 10, 2.5]],
                    // [ 'INSERT INTO Measurements (pers_id, meas_date, lat, lon, meas_RN, meas_val, meas_err, meas_MDA) VALUES (?,?,?,?,?,?,?,?)', [3, '2019-01-05T00:00', 1.5, 1.5, 'I131', 2.5, 10, 2.7]],
                    // [ 'INSERT INTO Measurements (pers_id, meas_date, lat, lon, meas_RN, meas_val, meas_err, meas_MDA) VALUES (?,?,?,?,?,?,?,?)', [3, '2019-01-06T00:00', 1.5, 1.5, 'I131', 2, 10, 3]],
                    // [ 'INSERT INTO defIntakeShape (int_date, int_RN, int_val) VALUES (?,?,?)', ['2019-01-01T00:05', 'I131', 2.5]],
                    // [ 'INSERT INTO defIntakeShape (int_date, int_RN, int_val) VALUES (?,?,?)', ['2019-01-02T00:05', 'I131', 5]],
                    // [ 'INSERT INTO defIntakeShape (int_date, int_RN, int_val) VALUES (?,?,?)', ['2019-01-03T00:05', 'I131', 1]],
                    // [ 'INSERT INTO IntakeShapes (pers_id, int_date, int_RN, int_val) VALUES (?,?,?,?)', [1, '2019-01-01T03:01', 'I131', 0]],
                    // [ 'INSERT INTO IntakeShapes (pers_id, int_date, int_RN, int_val) VALUES (?,?,?,?)', [1, '2019-01-01T12:05', 'I131', 0.1]],
                    // [ 'INSERT INTO IntakeShapes (pers_id, int_date, int_RN, int_val) VALUES (?,?,?,?)', [1, '2019-01-02T00:05', 'I131', 3]],
                    // [ 'INSERT INTO IntakeShapes (pers_id, int_date, int_RN, int_val) VALUES (?,?,?,?)', [1, '2019-01-02T19:12', 'I131', 0.5]],
                    // [ 'INSERT INTO IntakeShapes (pers_id, int_date, int_RN, int_val) VALUES (?,?,?,?)', [1, '2019-01-03T12:00', 'I131', 1]],
                    // [ 'INSERT INTO IntakeShapes (pers_id, int_date, int_RN, int_val) VALUES (?,?,?,?)', [1, '2019-01-04T00:00', 'I131', 0]],
                    // [ 'INSERT INTO IntakeShapes (pers_id, int_date, int_RN, int_val) VALUES (?,?,?,?)', [2, '2019-01-01T03:01', 'I131', 0]],
                    // [ 'INSERT INTO IntakeShapes (pers_id, int_date, int_RN, int_val) VALUES (?,?,?,?)', [2, '2019-01-01T12:05', 'I131', 0.1]],
                    // [ 'INSERT INTO IntakeShapes (pers_id, int_date, int_RN, int_val) VALUES (?,?,?,?)', [2, '2019-01-02T00:05', 'I131', 3]],
                    // [ 'INSERT INTO IntakeShapes (pers_id, int_date, int_RN, int_val) VALUES (?,?,?,?)', [2, '2019-01-02T19:12', 'I131', 0.5]],
                    // [ 'INSERT INTO IntakeShapes (pers_id, int_date, int_RN, int_val) VALUES (?,?,?,?)', [2, '2019-01-03T12:00', 'I131', 1]],
                    // [ 'INSERT INTO IntakeShapes (pers_id, int_date, int_RN, int_val) VALUES (?,?,?,?)', [2, '2019-01-04T00:00', 'I131', 0]],
                    // [ 'INSERT INTO IntakeShapes (pers_id, int_date, int_RN, int_val) VALUES (?,?,?,?)', [2, '2019-01-01T03:01', 'I132', 0]],
                    // [ 'INSERT INTO IntakeShapes (pers_id, int_date, int_RN, int_val) VALUES (?,?,?,?)', [2, '2019-01-01T12:05', 'I132', 0.1]],
                    // [ 'INSERT INTO IntakeShapes (pers_id, int_date, int_RN, int_val) VALUES (?,?,?,?)', [2, '2019-01-02T00:05', 'I132', 3]],
                    // [ 'INSERT INTO IntakeShapes (pers_id, int_date, int_RN, int_val) VALUES (?,?,?,?)', [2, '2019-01-02T19:12', 'I132', 0.5]],
                    // [ 'INSERT INTO IntakeShapes (pers_id, int_date, int_RN, int_val) VALUES (?,?,?,?)', [2, '2019-01-03T12:00', 'I132', 1]],
                    // [ 'INSERT INTO IntakeShapes (pers_id, int_date, int_RN, int_val) VALUES (?,?,?,?)', [2, '2019-01-04T00:00', 'I132', 0]],
                    // [ 'INSERT INTO IntakeShapes (pers_id, int_date, int_RN, int_val) VALUES (?,?,?,?)', [3, '2019-01-01T03:01', 'I131', 1]],
                    // [ 'INSERT INTO defPrm (id, dtAccident) VALUES (?,?)', [1, '2019-01-01T00:00']]
                ], function() {
                        dispatch('fillPersons');
                        // console.log('Before read def');
                        dispatch('getDefPrm');
                    }, function(error) {
                        console.error('SQL batch ERROR: ' + error.message);
                    }
                );
            }, function(err){
                console.error('DB problem');
            });
        },
        addPerson({state, commit, dispatch}, calcs) {
            let rec = Object.assign({}, state.currentPerson);
            let sql = '';
            let args = [];
            if(calcs) {
                sql = 'INSERT INTO Persons (name, surname, birthday, sex, phone, pregnant, fetAge, intakePath, useI132, useFetus, calculated, ' +
                          'I131MOPScaleCoef, I131MOPDose, I131FetusScaleCoef, I131FetusDose, I132MOPScaleCoef, I132MOPDose, I132FetusScaleCoef, I132FetusDose ' +
                          ') VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);' 
                args = [rec.name, rec.surname, rec.birthday, rec.sex, rec.phone, rec.pregnant, rec.fetAge, calcs.calcPrm.intakePath, 
                            calcs.calcPrm.useI132 ? 1 : 0, calcs.calcPrm.useFetus ? 1 : 0, calcs.calcRes.calculated ? 1 : 0]; 
                let args2 = [0, 0, 0, 0, 0, 0, 0, 0];
                if (calcs.calcRes.calculated) {
                    args2 = [calcs.calcRes['I131']['MOP'].scaleCoef, calcs.calcRes['I131']['MOP'].dose,
                            calcs.calcRes['I131']['Fetus'].scaleCoef, calcs.calcRes['I131']['Fetus'].dose, 
                            calcs.calcRes['I132']['MOP'].scaleCoef,  calcs.calcRes['I132']['MOP'].dose, 
                            calcs.calcRes['I132']['Fetus'].scaleCoef, calcs.calcRes['I132']['Fetus'].dose];
                }
                args = args.concat(args2);
            } else {
                sql = 'INSERT INTO Persons (name, surname, birthday, sex, phone, pregnant, fetAge) VALUES (?,?,?,?,?,?,?);' 
                args = [rec.name, rec.surname, rec.birthday, rec.sex, rec.phone, rec.pregnant, rec.fetAge];
            }
            state.dbf.executeSql(sql, args, 
                function (resultSet){
                    rec.id = resultSet.insertId;
                    commit('addPerson', rec);
                    if (state.currentPhotoName && !/^id_/.test(state.currentPhotoName)) {
                        let fname = 'id_' + rec.id + '.jpg';
                        dispatch('datafiles/renameFile',{fromFile: state.currentPhotoName, toFile: fname},{root: true});
                    }
                    commit('setCurrentPhotoName', '');
                }, 
                function() {
                    console.log('SQL INSERT ERROR')
                }
            );
        },
        getPerson({state, commit, dispatch}, key) {
            // commit('setCurrentPersonId', key);
            if (key>=0) {
                if (key in state.persons) {
                    state.dbf.executeSql('SELECT * FROM Persons WHERE id = ?;', [key], 
                    function (resultSet){
                        let rec = resultSet.rows.item(0);
                        // console.log(rec);
                        rec.useFetus = rec.useFetus == 1;
                        rec.useI132 = rec.useI132 == 1;
                        rec.calculated = rec.calculated == 1;
                        // console.log(rec);
                        commit('fillCurrentPerson', rec);
                        commit('setImgPrefix',null,{root: true});
                        dispatch('getMeasurements', key);
                        dispatch('getIntakeShape', key);
                        commit('calc/fillCalculationsfromPers', rec, {root: true});
                    }, 
                    function() {
                        console.error('ERROR in SQL SELECT for edit Person record with id=' + key);
                    });
                } else {
                    commit('clearCurrentPerson');
                }
            }
        },
        editPerson({state, commit, dispatch}, calcs) {
            let rec = state.currentPerson;
            let sql = '';
            let args = [];
            if(calcs) {
                sql = 'UPDATE Persons SET name = ?, surname = ?, birthday = ?, sex = ?, phone = ?, pregnant = ?, fetAge = ?, intakePath = ?, useI132 = ?, useFetus = ?, calculated = ?, ' +
                          'I131MOPScaleCoef = ?, I131MOPDose = ?, I131FetusScaleCoef = ?, I131FetusDose = ?, I132MOPScaleCoef = ?, I132MOPDose = ?, I132FetusScaleCoef = ?, I132FetusDose = ? ' +
                          'WHERE id = ?;' 
                args = [rec.name, rec.surname, rec.birthday, rec.sex, rec.phone, rec.pregnant, rec.fetAge, calcs.calcPrm.intakePath, 
                        calcs.calcPrm.useI132 ? 1 : 0, calcs.calcPrm.useFetus ? 1 : 0, calcs.calcRes.calculated ? 1 : 0]; 
                let args2 = [0, 0, 0, 0, 0, 0, 0, 0, rec.id];
                if (calcs.calcRes.calculated) {
                    args2 = [calcs.calcRes['I131']['MOP'].scaleCoef, calcs.calcRes['I131']['MOP'].dose,
                            calcs.calcRes['I131']['Fetus'].scaleCoef, calcs.calcRes['I131']['Fetus'].dose, 
                            calcs.calcRes['I132']['MOP'].scaleCoef,  calcs.calcRes['I132']['MOP'].dose, 
                            calcs.calcRes['I132']['Fetus'].scaleCoef, calcs.calcRes['I132']['Fetus'].dose, rec.id];
                }
                args = args.concat(args2);
            } else {
                sql = 'UPDATE Persons SET name = ?, surname = ?, birthday = ?, sex = ?, phone = ?, pregnant = ?, fetAge = ? WHERE id = ?;' 
                args = [rec.name, rec.surname, rec.birthday, rec.sex, rec.phone, rec.pregnant, rec.fetAge]; 
            }

            state.dbf.executeSql(sql, args,
                function (resultSet){
                    // rec.id = resultSet.insertId;
                    commit('setPerson', rec);
                    if (state.currentPhotoName && !/^id_/.test(state.currentPhotoName)) {
                        let fname = 'id_' + rec.id + '.jpg';
                        dispatch('datafiles/renameFile',{fromFile: state.currentPhotoName, toFile: fname},{root: true});
                    } 
                    commit('setCurrentPhotoName', '');
                }, 
                function() {
                    console.error('SQL UPDATE ERROR');
                }
            );
        },
        deletePerson({state, commit, dispatch}, key) {
            // console.log(key);
            state.dbf.executeSql('DELETE FROM Persons WHERE id = ?', [key], 
            function (resultSet) {
                commit('deletePerson', key);
                let fname = 'id_' + key + '.jpg';
                dispatch('datafiles/deleteFile', fname,{root: true});
            }, 
            function() {
                console.log('SQL DELETE ERROR');
            });
        },
        fillPersons({state,commit}) {
            commit('clearPersons');
            state.dbf.executeSql('SELECT * FROM Persons;', [], 
                function (resultSet) {
                    for(let i=0;i<resultSet.rows.length; i++) {
                        commit('addPerson', resultSet.rows.item(i));
                        // Vue.set(state.persons, resultSet.rows.item(i).id, resultSet.rows.item(i));
                    }
                }, 
                function() {console.log('SQL SELECT ERROR')}
            );
        },
        getMeasurements({state, commit}, key) {
            state.dbf.executeSql('SELECT * FROM Measurements WHERE pers_id = ? ORDER BY meas_date;', [key],
            function (measResultSet){
                commit('fillCurrentPersonMeasurements', measResultSet.rows);
                commit('calc/setMeasurements',state.currentPerson.measurements,{root: true});
            },
            function() {
                console.log('ERROR in SQL SELECT for edit Measurements records with id=' + key);
            });
        },
        getMeasurement({commit, state}, key) {
            if (key>=0) {
                state.dbf.executeSql('SELECT * FROM Measurements WHERE id = ?;', [key], 
                function (resultSet) {
                    commit('fillCurrentMeasurement', resultSet.rows.item(0));
                }, 
                function() {
                    console.log('ERROR in SQL SELECT for edit Measurement record with id=' + key);
                }
            );}
        },
        addMeasurement({state, dispatch}, rec) {
            state.dbf.executeSql('INSERT INTO Measurements (pers_id, meas_date, lat, lon, meas_RN, meas_val, meas_err, meas_MDA) VALUES (?,?,?,?,?,?,?,?)', 
                [state.currentPerson.id, rec.meas_date, Number(rec.lat), Number(rec.lon), rec.meas_RN, Number(rec.meas_val), Number(rec.meas_err), Number(rec.meas_MDA)],
                function (resultSet){
                    rec.id = resultSet.insertId;
                    dispatch('getMeasurements', state.currentPerson.id);
                }, 
                function() {
                    console.log('SQL Measurements INSERT ERROR');
                }
            );
        },
        editMeasurement({state, dispatch}, rec) {
            state.dbf.executeSql('UPDATE Measurements SET meas_date = ?, lat = ?, lon = ?, meas_RN = ?, meas_val = ?, meas_err = ?, meas_MDA = ? WHERE id = ?;', 
                [rec.meas_date, Number(rec.lat), Number(rec.lon), rec.meas_RN, Number(rec.meas_val), Number(rec.meas_err), Number(rec.meas_MDA), rec.id], 
                function (resultSet){
                    // commit('setMeasurement', rec);
                    dispatch('getMeasurements', state.currentPerson.id);
                }, 
                function() {
                    console.log('SQL Measurements UPDATE ERROR');
                }
            );
        },
        deleteMeasurement({state, commit}, key) {
            // console.log(key);
            state.dbf.executeSql('DELETE FROM Measurements WHERE id = ?', [key], 
            function (resultSet) {
                commit('deleteMeasurement', key);
            }, 
            function() {
                console.log('SQL Measurement DELETE ERROR');
            });
        },
        getIntakeShape({state, commit, rootState}, key) {
            state.dbf.executeSql('SELECT * FROM IntakeShapes WHERE pers_id = ? ORDER BY int_date;', [key],
            function (ISResultSet){
                commit('fillCurrentPersonIntakeShapes', ISResultSet.rows);
                commit('calc/setIntakeShape', {dtAccident: rootState.dtAccident, IS: state.currentPerson.intakeShape} ,{root: true});
            },
            function() {
                console.error('ERROR in SQL SELECT for edit IntakeShape records with id=' + key);
            });
        },
        addIntakeShapePoint({state, dispatch}, rec) {
            state.dbf.executeSql('INSERT INTO IntakeShapes (pers_id, int_date, int_RN, int_val) VALUES (?,?,?,?)', 
                [state.currentPerson.id, rec.int_date, rec.int_RN, Number(rec.int_val)],
                function (resultSet){
                    // rec.id = resultSet.insertId;
                    dispatch('getIntakeShape', state.currentPerson.id);
                }, 
                function() {
                    console.error('SQL IntakeShape INSERT ERROR');
                }
            );
        },
        editIntakeShapePoint({state, dispatch}, rec) {
            state.dbf.executeSql('UPDATE IntakeShapes SET int_date = ?, int_RN = ?, int_val = ? WHERE id = ?;', 
                [rec.int_date, rec.int_RN, Number(rec.int_val), rec.id], 
                function (resultSet){
                    dispatch('getIntakeShape', state.currentPerson.id);
                }, 
                function() {
                    console.error('SQL IntakeShape UPDATE ERROR');
                }
            );
        },
        deleteIntakeShapePoint({state, commit}, key) {
            state.dbf.executeSql('DELETE FROM IntakeShapes WHERE id = ?', [key], 
            function (resultSet) {
                commit('deleteISPoint', key);
            }, 
            function() {
                console.error('SQL IntakeShapes DELETE ERROR');
            });
        },
        copyDefIntakeShapePoint({state, dispatch}, data) {
            state.dbf.executeSql('DELETE FROM IntakeShapes WHERE pers_id = ? AND int_RN = ?', [data.key, data.RN], 
            function (resultSet) {
                state.dbf.executeSql('INSERT INTO IntakeShapes (pers_id, int_date, int_RN, int_val) SELECT ? AS pers_id, int_date, int_RN, int_val FROM defIntakeShape WHERE int_RN = ? ORDER BY int_date;', [data.key, data.RN],
                function (ISResultSet){
                    // console.log('defIS copy', ISResultSet);
                    dispatch('getIntakeShape', data.key);
                },
                function(err) {
                    console.error('ERROR in SQL INSERT INTO IntakeShapes', err);
                });
            }, 
            function() {
                console.error('SQL IntakeShapes DELETE ERROR');
            });
        },
        getDefPrm({state, commit}) {
            state.dbf.executeSql('SELECT * FROM defIntakeShape ORDER BY int_date;', [],
            function (ISResultSet){
                // console.log('defIS read', ISResultSet.rows);
                commit('fillDefIS', ISResultSet.rows);
            },
            function() {
                console.error('ERROR in SQL SELECT for edit defIntakeShape');
            });

            state.dbf.executeSql('SELECT * FROM defPrm WHERE id=1;', [],
            function (ISResultSet){
                commit('setDtAccident', ISResultSet.rows.item(0).dtAccident, {root: true});
            },
            function() {
                console.error('ERROR in SQL SELECT for edit defIntakeShape');
            });
        },
        savePrm ({state, commit}, prm) {
            state.dbf.executeSql('UPDATE defPrm SET dtAccident = ? WHERE id = 1;', 
                [prm.dtAccident], 
                function (resultSet){
                    commit('setDtAccident', prm.dtAccident, {root: true});
                }, 
                function() {
                    console.error('SQL IntakeShape UPDATE ERROR');
                }
            );
        },
        saveDefIS ({state, dispatch}, recs) {
            state.dbf.executeSql('DELETE FROM defIntakeShape', [], 
            function (resultSet) {
                let qarr = [];
                for(let rec of recs) {
                    qarr.push(['INSERT INTO defIntakeShape (int_date, int_RN, int_val) VALUES (?,?,?)', [rec.int_date, rec.int_RN, rec.int_val]]);
                }
                state.dbf.sqlBatch(qarr,
                function() {
                    dispatch('getDefPrm');
                }, function(error) {
                    console.error('SQL batch ERROR: ' + error.message);
                });
            }, 
            function() {
                console.error('SQL saveDefIS ERROR');
            });
        }
    }
};

