export default {
    namespaced: true,
    state: {
        fs: null,
        photoDir: null
    },
    getters: {
        fs(state) {
            return state.fs;
        },
        photoDir(state) {
            return state.photoDir;
        }
    },
    mutations: {
        setFS(state, fs) {
            state.fs = fs;
        },
        setPhotoDir(state, dir) {
            state.photoDir = dir;
            // console.log('photoDir: ' + dir);
        }
    },
    actions: {
        errorHandle(err) {
            console.log('datafiles ERROR: ' + err);
        },
        openFS({commit, dispatch}) {
            window.resolveLocalFileSystemURL(cordova.file.externalApplicationStorageDirectory, function (fs) {
                commit('setFS', fs);
                fs.getDirectory('files/Photo', { create: true }, function (dirEntry) {
                    commit('setPhotoDir', dirEntry);
                    commit('setActive',null,{root: true});
                }, function (err) {
                    dispatch('errorHandle', err);
                });
            }, function (err) {
                dispatch('errorHandle', err);
            });
        },
        deleteFile({state}, fname) {
            if (fname) {
                // console.log('Removing file ' + fname + '!!!');
                state.photoDir.getFile(fname, {create: false}, 
                    (fileEntry) => {
                        fileEntry.remove(
                            (file) => {
                                // console.log('file ' + file + ' removed');
                            },
                            (err) => {
                                console.log('ERROR ' + err);
                            },
                            () => {
                                console.log('file ' + fname + ' is not found');
                            }
                        )
                    }, 
                    (err) => {
                    console.log('file ' + this.currentPhotoName + ' is not found');
                    }
                );
            }
        },
        renameFile({state, commit}, fdata) { //console.log('Renaming:'); console.log(fdata);
            state.photoDir.getFile(fdata.fromFile, {create: false}, 
                (fileEntry) => { //console.log(fdata);
                    fileEntry.moveTo(state.photoDir, fdata.toFile,
                        (file) => {
                            commit('setImgPrefix',null,{root: true});
                            // console.log('file ' + file.name + ' renamed');
                        },
                        (err) => {
                            console.log('ERROR ' + err);
                        }
                    );        
                }, 
                (err) => {
                  console.log('Renamed file ' + fdata.fromFile + ' is not found');
                }
            );
        }
    }    
};