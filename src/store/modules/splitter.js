export default {
    namespaced: true,
    state: {
        open: false
    },
    mutations: {
        toggle(state, shouldOpen){
            if (shouldOpen instanceof Boolean) {
                state.open = shouldOpen;
            } else {
                state.open = !state.open;
            }
        }
    }
};