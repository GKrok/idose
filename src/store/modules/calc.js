import tbl_dpc_i131_mop_inhal from 'assets/json/DPC_I131_MOP_Inhalation.json';
import tbl_dpc_i131_mop_ingest from 'assets/json/DPC_I131_MOP_Ingestion.json';
import tbl_dpc_i131_fetus_inhal from 'assets/json/DPC_I131_Fetus_Inhalation.json';
import tbl_dpc_i131_fetus_ingest from 'assets/json/DPC_I131_Fetus_Ingestion.json';
import tbl_dpc_i132_mop_inhal from 'assets/json/DPC_I132_MOP_Inhalation.json';
import tbl_dpc_i132_mop_ingest from 'assets/json/DPC_I132_MOP_Ingestion.json';
import tbl_dpc_i132_fetus_inhal from 'assets/json/DPC_I132_Fetus_Inhalation.json';
import tbl_dpc_i132_fetus_ingest from 'assets/json/DPC_I132_Fetus_Ingestion.json';
// import tbl_dose_i131_mop from 'assets/json/Dose_I131_MOP.json';
// import tbl_dose_i132_mop from 'assets/json/Dose_I132_MOP.json';
// import tbl_dose_i131_Fetus from 'assets/json/Dose_I131_Fetus.json';
// import tbl_dose_i132_Fetus from 'assets/json/Dose_I132_Fetus.json';
import tbl_doses from 'assets/json/Doses.json';
import tbl_rn_prm from 'assets/json/RNPrm.json';
import moment from 'moment';

function gr_TestSign(Value1, Value2) {
    let res = 0;
    if (!((Value1 === 0) || (Value2 === 0))) {
        res = (Math.sign(Value1) * Math.sign(Value2));
    }
    return res;
}

function gr_getFetusAgeGroup(age) {
    let res = 0;
    if (age<2) {
        res = 1;
    } else if (age<8) {
        res = 2;
    } else if (age<12) {
        res = 3;
    } else if (age<20) {
        res = 4;
    } else if (age<30) {
        res = 5;
    } else {
        res = 6;
    }
    return res;
}

function gr_getAgeGroup(years, sex) {
    let res = 6;
    if (years<1) {
        res = 1;
    } else if (years<2) {
        res = 2;
    } else if (years<7) {
        res = 3;
    } else if (years<12) {
        res = 4;
    } else if (years<17) {
        res = 5;
    } else {
        if (sex=='M') {
            res = 6;
        } else {
            res = 7;
        }
    }
    return res;
}

function gr_calcSplineCoeff(vals) {
    let len=vals.length;
    let coeffs = []; 

    if (len<2) return;
    let iData1 = 0;
    let iData2 = 1;

    let h1 = vals[iData2][0] - vals[iData1][0];
    let d1 = (vals[iData2][1] - vals[iData1][1])/h1;
    if (len>2) {
        let iData3 = 2;
        let h2 = vals[iData3][0] - vals[iData2][0];
        let d2 = (vals[iData3][1] - vals[iData2][1])/h2;
        let w1 = 1 + h1 / (h1 + h2);
        coeffs[iData1] = {
            c1: w1*d1 + (1-w1)*d2,
            c2: 0,
            c3: 0
        };
        if (gr_TestSign(coeffs[iData1].c1, d1)<=0) {
            coeffs[iData1].c1 = 0
        } else {
            if(gr_TestSign(d1, d2)<0) {
                let dMax = 3*d1;
                if(Math.abs(coeffs[iData1].c1)>Math.abs(dMax)) {
                    coeffs[iData1].c1 = dMax;
                }
            }
        }
        for(let i=1; i<len-1; i++) {
            if(i>1) {
                iData1 = iData2;
                iData2 = iData3;
                iData3++;
                h1 = h2;
                d1 = d2;
                h2 = vals[iData3][0] - vals[iData2][0];
                if(h2==0) {
                    d2 = 0;
                } else {
                    d2 = (vals[iData3][1] - vals[iData2][1])/h2;
                }
            }    
            coeffs[iData2] = {c1:0, c2: 0, c3: 0};
            if(gr_TestSign(d1, d2)>0) {
                if((h1+h2)!=0) {
                    w1 = (1 + h1 / (h1 + h2)) / 3;
                    coeffs[iData2].c1 = 1 / (w1 / d2 + (1 - w1) / d1);
                }
            }
            if (h1==0) {
                coeffs[iData1].c2 = 0;
                coeffs[iData1].c3 = 0;
            } else {
                coeffs[iData1].c2 = (3 * d1 - 2*coeffs[iData1].c1 - coeffs[iData2].c1) / h1;
                coeffs[iData1].c3 = (coeffs[iData1].c1 + coeffs[iData2].c1 - 2*d1) / (h1*h1);                    
            }
        }
        w1 = -h2 / (h1 + h2);
        coeffs[iData3] = {
            c1: w1 * d1 + (1 - w1) * d2,
            c2: 0,
            c3: 0
        };
        if (gr_TestSign(coeffs[iData3].c1, d2) <= 0) {
            coeffs[iData3].c1 = 0;
        } else {
            if (gr_TestSign(d1, d2) < 0) {
                let dMax = 3 * d2;
                if (Math.abs(coeffs[iData3].c1) > Math.abs(dMax)) {
                    coeffs[iData3].c1 = dMax;
                }
            }
        }
        coeffs[iData2].c2 = (3 * d2 - 2 * coeffs[iData2].c1 - coeffs[iData3].c1) / h2;
            coeffs[iData2].c3 = (coeffs[iData2].c1 + coeffs[iData3].c1 - 2 * d2) / (h2*h2);
    } else {
        coeffs[iData1] = {c1: d1, c2: 0, c3: 0};
        coeffs[iData2] = {c1: d1, c2: 0, c3: 0};
    }
    return coeffs;
}

function gr_calcSpline(data, t) {
    if(t<data.vals[0][0] || t>data.vals[data.vals.length-1][0]) {
        return 0;
    }
    let ind = data.vals.length-1;
    for(let i=0; i<data.vals.length; i++) {
        if(data.vals[i][0] > t) {
            ind = i-1;
            break;
        }
    }
    if (ind<0) {ind=0}
    let HH = t - data.vals[ind][0];
    return ((data.coeffs[ind].c3 * HH + data.coeffs[ind].c2) * HH + data.coeffs[ind].c1) * HH + data.vals[ind][1];
}

function gr_calcIntegral(data, t) {
    let Res = 0;
    
    if(t<data.vals[0][0]) {return 0}
    let ind = data.vals.length-1;
    for(let i=0; i<data.vals.length; i++) {
        if(data.vals[i][0] > t) {
            ind = i-1;
            break;
        }
    }
    if (ind<0) {return 0}
    
    for(let i=0;i<ind;i++) {
        let HH = data.vals[i+1][0] - data.vals[i][0];
        Res += (data.vals[i][1] + data.vals[i+1][1])*HH/2 + (data.coeffs[i].c1 - data.coeffs[i+1].c1)*HH*HH/12;
    }
    
    if(ind<(data.vals.length-1)) {
        let HHX = t - data.vals[ind][0];
        let HH = data.vals[ind+1][0] - data.vals[ind][0];
        Res += HHX * (data.vals[ind][1] + HHX * (data.coeffs[ind].c1/2 + HHX * ((3 * (data.vals[ind+1][1] - data.vals[ind][1])/(HH*HH) - (data.coeffs[ind+1].c1 + 2 * data.coeffs[ind].c1) / HH)/3 + 
            HHX * (((data.coeffs[ind+1].c1 + data.coeffs[ind].c1) / (HH*HH) - 2*(data.vals[ind+1][1] - data.vals[ind][1]) / (HH*HH*HH)) / 4))));
    }
    return Res;
}

function gr_calcExpIntegral(data, lambda) {
    if (data && data.vals && data.vals.length>0) {
        let res = 0;
        // console.log(data, lambda);
        for(let i=0;i<data.vals.length-1;i++) {
            let HH = data.vals[i+1][0]-data.vals[i][0];
            // let t = data.vals[i][0] - data.vals[0][0];
            let E1 = Math.exp(lambda*data.vals[i][0]);
            // t = data.vals[i+1][0] - data.vals[0][0];
            let E2 = Math.exp(lambda*data.vals[i+1][0]);
            let F1 = data.vals[i][1];
            let F2 = data.vals[i+1][1];
            let D1 = data.coeffs[i].c1;
            let D2 = data.coeffs[i+1].c1;
            let C2 = data.coeffs[i].c2;
            let C3 = data.coeffs[i].c3;
            res += (F2*E2-F1*E1)/lambda + (E1*D1-E2*D2)/(lambda*lambda) + (2*(E2-E1)*(C2-3*C3/lambda) + 6*C3*HH*E2)/(lambda*lambda*lambda);
        }
        return res;
    } else {
        return 0;
    }
}

export default {
    namespaced: true,
    state: {
        DPCdata: {
            I131: {
                Inhal: {
                    MOP: tbl_dpc_i131_mop_inhal, 
                    Fetus: tbl_dpc_i131_fetus_inhal
                },
                Ingest: {
                    MOP: tbl_dpc_i131_mop_ingest,
                    Fetus: tbl_dpc_i131_fetus_ingest
                }
            },
            I132: {
                Inhal: {
                    MOP: tbl_dpc_i132_mop_inhal,
                    Fetus: tbl_dpc_i132_fetus_inhal
                },
                Ingest: {
                    MOP: tbl_dpc_i132_mop_ingest,
                    Fetus: tbl_dpc_i132_fetus_ingest
                }
            }
        },
        RNinfo: tbl_rn_prm,
        // DoseCoeffs: {
        //     I131: {
        //         MOP: tbl_dose_i131_mop, 
        //         Fetus: tbl_dose_i131_Fetus
        //     },
        //     I132: {
        //         MOP: tbl_dose_i132_mop,
        //         Fetus: tbl_dose_i132_Fetus
        //     }
        // },
        Doses: tbl_doses,
        calcPrm: {
            useI132: false,
            useFetus: false,
            // dtAccident: new Date('2000-01-01T00:00'),
            intakePath: 'Inhal',
            measurements: {},
            IS: {},
            zFunc: {},
            doseCoef: {},
            ageGroup: -1, 
            ageFetGroup: -1
        },
        calcRes: {
            calculated: false,
            I131: {
                MOP: {
                    scaleCoef: 0,
                    dose: 0,
                    approximation: []
                },
                Fetus: {
                    scaleCoef: 0,
                    dose: 0
                    // approximation: []
                }
            },
            I132: {
                MOP: {
                    scaleCoef: 0,
                    dose: 0,
                    approximation: []
                },
                Fetus: {
                    scaleCoef: 0,
                    dose: 0
                    // approximation: []
                }
            }
        },
        calcResTotal: {
            MOP: {
                th30: {I131: 0,I132: 0,I133: 0,I134: 0,I135: 0,Te132: 0,Total: 0},
                th70: {I131: 0,I132: 0,I133: 0,I134: 0,I135: 0,Te132: 0,Total: 0},
                Eff: {I131: 0,I132: 0,I133: 0,I134: 0,I135: 0,Te132: 0,Total: 0}
            },
            Fetus: {
                th30: {I131: 0,I132: 0,I133: 0,I134: 0,I135: 0,Te132: 0,Total: 0},
                th70: {I131: 0,I132: 0,I133: 0,I134: 0,I135: 0,Te132: 0,Total: 0},
                Eff: {I131: 0,I132: 0,I133: 0,I134: 0,I135: 0,Te132: 0,Total: 0}
            }    
        }
    },
    getters: {
        // dtAccident(state, getters, rootState) {
        //     return rootState.dtAccident;
        // },
        getDPC: (state) => (nuclide, intakePath, pers) => {
            return state.DPCdata[nuclide][intakePath][pers];
        },
        getDPCforAge: (state) => (nuclide, intakePath, pers, age) => {
            let res = [];
            let vals = state.DPCdata[nuclide][intakePath][pers];
            for(let rowval of vals) {
                res.push([rowval[0],rowval[age]]);
            }
            return res;
        },
        getDoseCoef: (state) => (nuclide, intakePath, pers, age) => {
            return state.Doses[pers][age-1][nuclide][intakePath][0];
        },
        // getDoseOfType: (state) => (RN, intakePath, pers, type) => {
        //     let srcDose = state.calcRes[RN][pers].dose;
        //     if (srcDose && srcDose>0) {
        //         let CIndX = -1;
        //         let ageInd = -1;
        //         if (pers=='MOP') {
        //             CIndX = 3;
        //             ageInd = state.calcPrm.ageGroup;
        //         } else {
        //             CIndX = 4;
        //             ageInd = state.calcPrm.ageFetGroup;
        //         }
        //         if (type=='Eff') {
        //             CIndX++;
        //         }
        //         let coeff = state.DoseCoeffs[RN][pers][intakePath][ageInd-1][CIndX];
        //         return srcDose*coeff;
        //     }
        //     else {
        //         return 0
        //     }
        // },
        getDoseCoefByRN: (state) => (nuclide, intakePath, pers, ageGroup, typeInd) => {
            return state.Doses[pers][ageGroup][nuclide][intakePath][typeInd];
        },
        getIntegralResponse: (state, getters, rootState) => (Mdt, RN, pers) => {
            let res = 0;
            let t = moment(Mdt).diff(rootState.dtAccident, 'days', true);
            if (state.calcPrm.IS[RN].vals.length>1) {
                let vals=[];
                let NSteps = 10;
                for(let i=0;i<(state.calcPrm.IS[RN].vals.length-1);i++) {
                    if (state.calcPrm.IS[RN].vals[i][0]>t) break;
                    let HH = (state.calcPrm.IS[RN].vals[i+1][0] - state.calcPrm.IS[RN].vals[i][0]);
                    let step = HH/NSteps;
                    for(let j=0;j<NSteps;j++) {
                        let tIS = state.calcPrm.IS[RN].vals[i][0] + j*step;
                        let tZ = t - tIS;
                        if (tZ<0.125) break;
                        let valZ = gr_calcSpline(state.calcPrm.zFunc[RN][pers], tZ);
                        let valIS = gr_calcSpline(state.calcPrm.IS[RN], tIS);
                        let val = valIS / valZ;
                        vals.push([tIS, val]);
                    }
                }
                let tIS = state.calcPrm.IS[RN].vals[state.calcPrm.IS[RN].vals.length-1][0];
                let tZ = t - tIS;
                if (tZ>=0.125) {
                    let valZ = gr_calcSpline(state.calcPrm.zFunc[RN][pers], tZ);
                    let valIS = state.calcPrm.IS[RN].vals[state.calcPrm.IS[RN].vals.length-1][1];
                    let val = valIS / valZ;
                    vals.push([tIS, val]);
                }
                if (vals.length>0) {
                    let coeffs = gr_calcSplineCoeff(vals);
                    let intFunc = {vals, coeffs};
                    res = gr_calcIntegral(intFunc, t);
                } else {
                    res = 0;
                }    
            } else {
                let tIS = state.calcPrm.IS[RN].vals[0][0];
                let tZ = t - tIS;
                res = 1/gr_calcSpline(state.calcPrm.zFunc[RN][pers], tZ);
            }    
            return res;
        },
        calcParams(state) {
            return state.calcPrm;
        },
        calcResults(state) {
            return state.calcRes;
        },
        calcTotalResults(state) {
            let totres = {
                MOP: {
                    th30: {I131: 0,I132: 0,I133: 0,I134: 0,I135: 0,Te132: 0,Total: 0},
                    th70: {I131: 0,I132: 0,I133: 0,I134: 0,I135: 0,Te132: 0,Total: 0},
                    Eff: {I131: 0,I132: 0,I133: 0,I134: 0,I135: 0,Te132: 0,Total: 0}
                },
                Fetus: {
                    th30: {I131: 0,I132: 0,I133: 0,I134: 0,I135: 0,Te132: 0,Total: 0},
                    th70: {I131: 0,I132: 0,I133: 0,I134: 0,I135: 0,Te132: 0,Total: 0},
                    Eff: {I131: 0,I132: 0,I133: 0,I134: 0,I135: 0,Te132: 0,Total: 0}
                }    
            }
            for (let pers in totres) {
                for (let dose in totres[pers]) {
                    for (let rn in totres[pers][dose]) {
                        totres[pers][dose][rn] = state.calcResTotal[pers][dose][rn] * 1000;
                    }
                }
            }
            // console.log('getter',totres);
            return totres;
        }
    },
    mutations: {
        setCalcPrm(state, prm) {
            Object.assign(state.calcPrm, prm);
        },
        setScaleCoef(state, data) {
            state.calcRes[data.RN][data.pers].scaleCoef = data.coef;
        },
        setDose(state, data) {
            state.calcRes[data.RN][data.pers].dose = data.dose;
        },
        setCalculated(state, val) {
            state.calcRes.calculated = val;
        },
        setApproximation(state, data) {
            state.calcRes[data.prm.RN][data.prm.pers].approximation = data.res;
        },
        fillCalculationsfromPers(state, rec) {
            
            state.calcPrm.useI132 = rec.useI132;
            state.calcPrm.useFetus = rec.useFetus;
            // console.log(state.calcPrm.useI132, rec.useI132, state.calcPrm.useFetus, rec.useFetus);
            state.calcPrm.intakePath = rec.intakePath;
            state.calcRes.calculated = rec.calculated;
            state.calcRes.I131.MOP.scaleCoef = rec.I131MOPScaleCoef;
            state.calcRes.I131.MOP.dose = rec.I131MOPDose;
            state.calcRes.I131.MOP.approximation = [];
            state.calcRes.I131.Fetus.scaleCoef = rec.I131FetusScaleCoef;
            state.calcRes.I131.Fetus.dose = rec.I131FetusDose;
            state.calcRes.I131.Fetus.approximation = [];
            state.calcRes.I132.MOP.scaleCoef = rec.I132MOPScaleCoef;
            state.calcRes.I132.MOP.dose = rec.I132MOPDose;
            state.calcRes.I132.MOP.approximation = [];
            state.calcRes.I132.Fetus.scaleCoef = rec.I132FetusScaleCoef;
            state.calcRes.I132.Fetus.dose = rec.I132FetusDose;
            state.calcRes.I132.Fetus.approximation = [];
        },
        setMeasurements(state, meas) {
            state.calcPrm.measurements = {I131:[],I132:[]};
            for (let m of meas) {
                state.calcPrm.measurements[m.meas_RN].push(m);     
            }
        },
        setIntakeShape(state, data) {
            state.calcPrm.IS = {
                I131: {vals: [], coeffs: []},
                I132: {vals: [], coeffs: []}
            };
            for (let int of data.IS) {
                state.calcPrm.IS[int.int_RN].vals.push([moment(int.int_date).diff(data.dtAccident, 'days', true), int.int_val]);
            }
            if(state.calcPrm.IS['I131'].vals.length>0) {
                let IScoeffs = gr_calcSplineCoeff(state.calcPrm.IS['I131'].vals);
                state.calcPrm.IS['I131'].coeffs = IScoeffs;
            }
            if(state.calcPrm.IS['I132'].vals.length>0) {
                let IScoeffs = gr_calcSplineCoeff(state.calcPrm.IS['I132'].vals);
                state.calcPrm.IS['I132'].coeffs = IScoeffs;
            }
        },
        setPrmFuncs(state, data) {
            state.calcPrm.zFunc = data.zFunc;
            state.calcPrm.doseCoef = data.doseCoef;
            state.calcPrm.ageGroup = data.ageGroup;
            state.calcPrm.ageFetGroup = data.ageFetGroup;
        },
        setResTotal(state, data) {
            state.calcResTotal=data;
        },
        clearResTotal(state) {
            let data = {
                MOP: {
                    th30: {I131: 0,I132: 0,I133: 0,I134: 0,I135: 0,Te132: 0,Total: 0},
                    th70: {I131: 0,I132: 0,I133: 0,I134: 0,I135: 0,Te132: 0,Total: 0},
                    Eff: {I131: 0,I132: 0,I133: 0,I134: 0,I135: 0,Te132: 0,Total: 0}
                },
                Fetus: {
                    th30: {I131: 0,I132: 0,I133: 0,I134: 0,I135: 0,Te132: 0,Total: 0},
                    th70: {I131: 0,I132: 0,I133: 0,I134: 0,I135: 0,Te132: 0,Total: 0},
                    Eff: {I131: 0,I132: 0,I133: 0,I134: 0,I135: 0,Te132: 0,Total: 0}
                }
            }
            state.calcResTotal = data;
        }
    },
    actions: {
        setInnerCalcParams({state, getters, commit}, data) {
            let currDT = moment(data.firstDt);
            let diffDT = currDT.diff(data.birthday, 'years', true);
            let ageGroup = gr_getAgeGroup(diffDT, data.sex);
            let zFunc = {
                I131: {
                    MOP: {vals: [], coeffs: []},
                    Fetus: {vals: [], coeffs: []}
                },
                I132: {
                    MOP: {vals: [], coeffs: []},
                    Fetus: {vals: [], coeffs: []}
                }
            }
            zFunc['I131']['MOP'].vals = getters.getDPCforAge('I131', state.calcPrm.intakePath, 'MOP', ageGroup);
            zFunc['I131']['MOP'].coeffs = gr_calcSplineCoeff(zFunc['I131']['MOP'].vals);
            let ageFetGroup = 0;
            if (state.calcPrm.useFetus) {
                ageFetGroup = gr_getFetusAgeGroup(data.ageFetus);

                zFunc['I131']['Fetus'].vals = getters.getDPCforAge('I131', state.calcPrm.intakePath, 'Fetus', ageFetGroup);
                zFunc['I131']['Fetus'].coeffs = gr_calcSplineCoeff(zFunc['I131']['Fetus'].vals);
            }
            if(state.calcPrm.useI132) {
                zFunc['I132']['MOP'].vals = getters.getDPCforAge('I132', state.calcPrm.intakePath, 'MOP', ageGroup);
                zFunc['I132']['MOP'].coeffs = gr_calcSplineCoeff(zFunc['I132']['MOP'].vals);
                if (state.calcPrm.useFetus) {
                    zFunc['I132']['Fetus'].vals = getters.getDPCforAge('I132', state.calcPrm.intakePath, 'Fetus', ageFetGroup);
                    zFunc['I132']['Fetus'].coeffs = gr_calcSplineCoeff(zFunc['I132']['Fetus'].vals);
                }
            }

            let doseCoef = {
                I131: {
                    MOP: getters.getDoseCoef('I131', state.calcPrm.intakePath, 'MOP', ageGroup),
                    Fetus: 0,
                },
                I132: {
                    MOP: 0,
                    Fetus: 0
                }
            }
            
            if (state.calcPrm.useFetus) {
                doseCoef['I131']['Fetus'] = getters.getDoseCoef('I131', state.calcPrm.intakePath, 'Fetus', ageFetGroup);
            }

            if(state.calcPrm.useI132) {
                doseCoef['I132']['MOP'] = getters.getDoseCoef('I132', state.calcPrm.intakePath, 'MOP', ageGroup);
                if (state.calcPrm.useFetus) {
                    doseCoef['I132']['Fetus'] = getters.getDoseCoef('I132', state.calcPrm.intakePath, 'Fetus', ageFetGroup);
                }
            }
            commit('setPrmFuncs', {zFunc, doseCoef, ageGroup, ageFetGroup});
        },
        initCalcPrm({state, commit, dispatch, rootState}, prm) {
// prm = {
//      useI132: false,
//      useFetus: false,
//      birthday,
//      ageFetus: 0,
//      intakePath: 'Inhal', [Inhal, Ingest]
//      sex: 'M', [M, F]
//      measurements: [x,y],
//      IS: [x,y],
            commit('setMeasurements', prm.measurements);
            commit('setIntakeShape', {dtAccident: rootState.dtAccident, IS: prm.IS});
            // console.log('dtAccident', rootState.dtAccident);
            prm.useI132 = prm.useI132 && state.calcPrm.IS['I132'].vals.length>0 && state.calcPrm.measurements['I132'].length>0;
            prm.useFetus = prm.useFetus && (prm.sex=='F');

            let calcPrm = {
                useI132: prm.useI132,
                useFetus: prm.useFetus,
                intakePath: prm.intakePath,
            }
            commit('setCalcPrm', calcPrm);
            dispatch('setInnerCalcParams', {firstDt: prm.IS[0].int_date, birthday: prm.birthday, sex: prm.sex, ageFetus: prm.ageFetus});
        },
        calcDose({state, getters, commit}, data) {
//data.RN
//data.pers            
            commit('setCalculated', false);
            let NM = state.calcPrm.measurements[data.RN].length;
            let res = 0;
            let numer = 0;
            let denomin = 0;
            if(NM>0) {
                for(let MM of state.calcPrm.measurements[data.RN]) {
                    let RR = getters.getIntegralResponse(MM.meas_date,data.RN,data.pers);
                    let Err = MM.meas_err*MM.meas_val/100;
                    numer += RR*MM.meas_val/(Err*Err);
                    denomin += RR*RR/(Err*Err);
                }
                let d = state.calcPrm.doseCoef[data.RN][data.pers];
                res = numer/denomin/d;
            }
            commit('setScaleCoef', {RN:  data.RN, pers: data.pers, coef: res});
            // console.log(res);
            if(state.calcPrm.IS[data.RN].length>1) {
                numer = gr_calcIntegral(state.calcPrm.IS[data.RN], state.calcPrm.IS[data.RN].vals[state.calcPrm.IS[data.RN].vals.length-1][0]);
            } else {
                numer = 1;
            }    
            res *= numer*state.calcPrm.doseCoef[data.RN][data.pers];
            commit('setDose', {RN:  data.RN, pers: data.pers, dose: res});
            commit('setCalculated', true);
        },
        calcApproximation({state, getters, commit, rootState}, prm) {
// prm.minXval            
// prm.maxXval            
// prm.RN
// prm.pers
            let NSteps = 10;
            let dtmin=moment(prm.minXval);
            let dtmax=moment(prm.maxXval);
            let IS = state.calcPrm.IS[prm.RN];
            let dt=moment(rootState.dtAccident).add(IS.vals[0][0], 'days');
            let d = state.calcPrm.doseCoef[prm.RN][prm.pers];
            let scale = state.calcRes[prm.RN][prm.pers].scaleCoef;
            let res = [];
            if (dtmin.valueOf()<=dt.valueOf()) {
                dtmin = dt;
                res.push({x: dt.toISOString(), y: 0});
            }
            if (dtmax.valueOf()<dt.valueOf()) {
                commit('setApproximation', null);
            } else {
                let step = dtmax.diff(dtmin, 'h', true)/NSteps;
                dt = dtmin;
                if(res.length>0) { dt.add(step,'h') }

                for(let i = 0; i<=NSteps;i++, dt.add(step,'h')) {
                    // console.log(dt.toISOString(), prm)
                    let pnt = {x: dt.toISOString(), y: getters.getIntegralResponse(dt.toISOString(),prm.RN,prm.pers)*scale*d}
                    res.push(pnt);
                }
                commit('setApproximation', {prm, res});
            }
        },
        calcOtherDoses({state, getters, commit}) {
            let data = {
                MOP: {
                    th30: {I131: 0,I132: 0,I133: 0,I134: 0,I135: 0,Te132: 0,Total: 0},
                    th70: {I131: 0,I132: 0,I133: 0,I134: 0,I135: 0,Te132: 0,Total: 0},
                    Eff: {I131: 0,I132: 0,I133: 0,I134: 0,I135: 0,Te132: 0,Total: 0}
                },
                Fetus: {
                    th30: {I131: 0,I132: 0,I133: 0,I134: 0,I135: 0,Te132: 0,Total: 0},
                    th70: {I131: 0,I132: 0,I133: 0,I134: 0,I135: 0,Te132: 0,Total: 0},
                    Eff: {I131: 0,I132: 0,I133: 0,I134: 0,I135: 0,Te132: 0,Total: 0}
                }    
            };
            data.MOP.th30.I131 = state.calcRes.I131.MOP.dose;
            if(state.calcPrm.useFetus) {
                data.Fetus.th30.I131 = state.calcRes.I131.Fetus.dose;
            }
            let mixCoef = {
                MOP: {I131: 1,I132: 0,I133: 0,I134: 0,I135: 0,Te132: 0},
                Fetus: {I131: 1,I132: 0,I133: 0,I134: 0,I135: 0,Te132: 0}
            }
            let RNarr = ['I133', 'I134', 'I135'];
            if (!state.calcPrm.useI132) RNarr.push('Te132');
            let d_I131 = getters.getDoseCoefByRN('I131', state.calcPrm.intakePath, 'MOP', state.calcPrm.ageGroup-1, 0);
            let d_I131_fet = 0;
            if(state.calcPrm.useFetus) {
                d_I131_fet = getters.getDoseCoefByRN('I131', state.calcPrm.intakePath, 'Fetus', state.calcPrm.ageFetGroup-1, 0);
            }
            let isAcuteIntake = state.calcPrm.IS.I131.vals.length = 1;
            let int=1;
            if (!isAcuteIntake) {
                int = gr_calcIntegral(state.calcPrm.IS.I131, state.calcPrm.IS.I131.vals[state.calcPrm.IS.I131.length-1]);
            }
            
            for (let RN of RNarr) {
                let intExp = 1;
                if (!isAcuteIntake) {
                    intExp = gr_calcExpIntegral(state.calcPrm.IS.I131, state.RNinfo['I131'].lambda-state.RNinfo[RN].lambda);
                } else {
                    intExp = Math.exp((state.RNinfo['I131'].lambda-state.RNinfo[RN].lambda)*state.calcPrm.IS.I131.vals[0][0]);
                }
                let drn = getters.getDoseCoefByRN(RN, state.calcPrm.intakePath, 'MOP', state.calcPrm.ageGroup-1, 0);
                let drn_fet = 0;
                mixCoef.MOP[RN] = intExp*state.RNinfo[RN].kr*drn/(int*d_I131);
                if(state.calcPrm.useFetus) {
                    drn_fet = getters.getDoseCoefByRN(RN, state.calcPrm.intakePath, 'Fetus', state.calcPrm.ageFetGroup-1, 0);
                    mixCoef.Fetus[RN] = intExp*state.RNinfo[RN].kr*drn_fet/(int*d_I131_fet);
                }
            }
            if (!state.calcPrm.useI132) {
                let RN = 'I132';
                let intExp = 1;
                if (!isAcuteIntake) {
                    intExp = gr_calcExpIntegral(state.calcPrm.IS.I131, state.RNinfo['I131'].lambda-state.RNinfo['Te132'].lambda);
                } else {
                    intExp = Math.exp((state.RNinfo['I131'].lambda-state.RNinfo['Te132'].lambda)*state.calcPrm.IS.I131.vals[0][0]);
                }
                let drn = getters.getDoseCoefByRN(RN, state.calcPrm.intakePath, 'MOP', state.calcPrm.ageGroup-1, 0);
                let drn_fet = 0;
                mixCoef.MOP[RN] = intExp*state.RNinfo['Te132'].kr*drn/(int*d_I131);
                if(state.calcPrm.useFetus) {
                    drn_fet = getters.getDoseCoefByRN(RN, state.calcPrm.intakePath, 'Fetus', state.calcPrm.ageFetGroup-1, 0);
                    mixCoef.Fetus[RN] = intExp*state.RNinfo['Te132'].kr*drn_fet/(int*d_I131_fet);
                }
            }
            for(let RN in mixCoef.MOP) {
                data.MOP.th30[RN] = data.MOP.th30.I131 * mixCoef.MOP[RN];
                if(state.calcPrm.useFetus) {
                    data.Fetus.th30[RN] = data.Fetus.th30.I131 * mixCoef.Fetus[RN];
                }
            }
            if (state.calcPrm.useI132) {
                data.MOP.th30.Te132 = state.calcRes.I132.MOP.dose;
                let drnTe132 = getters.getDoseCoefByRN('Te132', state.calcPrm.intakePath, 'MOP', state.calcPrm.ageGroup-1, 0);
                let drnI132 = getters.getDoseCoefByRN('I132', state.calcPrm.intakePath, 'MOP', state.calcPrm.ageGroup-1, 0);
                data.MOP.th30.I132 = data.MOP.th30.Te132*1.03*drnI132/drnTe132;
                if(state.calcPrm.useFetus) {
                    data.Fetus.th30.Te132 = state.calcRes.I132.Fetus.dose;

                    drnTe132 = getters.getDoseCoefByRN('Te132', state.calcPrm.intakePath, 'Fetus', state.calcPrm.ageFetGroup-1, 0);
                    drnI132 = getters.getDoseCoefByRN('I132', state.calcPrm.intakePath, 'Fetus', state.calcPrm.ageFetGroup-1, 0);
                    data.Fetus.th30.I132 = data.Fetus.th30.Te132*1.03*drnI132/drnTe132;
                }
            }
            // console.log('Calc1', data);
            for (let RN in mixCoef.MOP) {
                let drn30 = getters.getDoseCoefByRN(RN, state.calcPrm.intakePath, 'MOP', state.calcPrm.ageGroup-1, 0);
                let drn70 = getters.getDoseCoefByRN(RN, state.calcPrm.intakePath, 'MOP', state.calcPrm.ageGroup-1, 1);
                let drnEff = getters.getDoseCoefByRN(RN, state.calcPrm.intakePath, 'MOP', state.calcPrm.ageGroup-1, 2);
                data.MOP.th70[RN] = data.MOP.th30[RN] * drn70 / drn30;
                data.MOP.Eff[RN] = data.MOP.th30[RN] * drnEff / drn30;
                if(state.calcPrm.useFetus) {
                    let drn30 = getters.getDoseCoefByRN(RN, state.calcPrm.intakePath, 'Fetus', state.calcPrm.ageFetGroup-1, 0);
                    let drn70 = getters.getDoseCoefByRN(RN, state.calcPrm.intakePath, 'Fetus', state.calcPrm.ageFetGroup-1, 1);
                    let drnEff = getters.getDoseCoefByRN(RN, state.calcPrm.intakePath, 'Fetus', state.calcPrm.ageFetGroup-1, 2);
                    data.Fetus.th70[RN] = data.Fetus.th30[RN] * drn70 / drn30;
                    data.Fetus.Eff[RN] = data.Fetus.th30[RN] * drnEff / drn30;
                }
                data.MOP.th30.Total += data.MOP.th30[RN];
                data.MOP.th70.Total += data.MOP.th70[RN];
                data.MOP.Eff.Total += data.MOP.Eff[RN];
                data.Fetus.th30.Total += data.Fetus.th30[RN];
                data.Fetus.th70.Total += data.Fetus.th30[RN];
                data.Fetus.Eff.Total += data.Fetus.Eff[RN];
            }
            // console.log('Calc2', data);
            commit('setResTotal', data);
        }
    }
};