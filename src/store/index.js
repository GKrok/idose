import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

if (process.env.NODE_ENV === 'development') {
    Vue.config.devtools = true;
}


import navigator from './modules/navigator';
import splitter from './modules/splitter';
// import tabbar from './modules/tabbar';
import db from './modules/db';
import datafiles from './modules/datafiles';
import calc from './modules/calc';
// import moment from 'moment';

export const store  = new Vuex.Store({
    state: {
        isAppActive: false,
        imgPrefix: new Date().toString(),
        // dtAccident: '2019-01-01T00:00'
        dtAccident: '2019-01-01T00:00'
    },
    getters: {
        isAppActive(state) {
            return state.isAppActive;
        },
        imgPrefix(state) {
            return state.imgPrefix;
        },
        dtAccident(state) {
            return state.dtAccident;
        }
    },
    mutations: {
        setActive(state) {
            state.isAppActive = true;
        },
        setImgPrefix(state) {
            let old = state.imgPrefix;
            while (old == state.imgPrefix) {state.imgPrefix = new Date().toString();}
        },
        setDtAccident(state, val) {
            state.dtAccident = val;
        }
    },
    modules: {
        navigator,
        splitter,
        // tabbar,
        db,
        datafiles,
        calc
    },
    strict: process.env.NODE_ENV !== 'production'
});