'use strict';
const grutils = {
    getDate(sdt) {
        let dt = new Date(sdt);
        // return dt.toLocaleDateString();

        let day = dt.getDate().toString();
        let month = (dt.getMonth() + 1).toString();
        let year = dt.getFullYear().toString();
        month = month.length < 2 ? '0' + month : month;
        day = day.length < 2 ? '0' + day : day;
        return year+"-"+month+"-"+day;
    },
    getTime(sdt) {
        let dt = new Date(sdt);
        // return dt.toLocaleTimeString();
         let hh = dt.toUTCString();
         return hh.split(' ')[4];
    }
};

export default grutils;