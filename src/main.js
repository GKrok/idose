import devtools from '@vue/devtools';
import Vue from 'vue';
// import Vuex from 'vuex';
import VueOnsen from 'vue-onsenui';

import {store} from './store';
import 'onsenui/css/onsenui.css';
import 'onsenui/css/onsen-css-components.css';


// Import App Custom Styles
import AppStyles from './assets/sass/main.scss'
import AppNavigator from './AppNavigator.vue';
import { inherits } from 'util';
import ScatterPlot from './components/ScatterPlot.vue';
import moment from 'moment';

Vue.use(VueOnsen);
Vue.component('scatter-plot', ScatterPlot);
Vue.filter('formatDateTime', function(value) {
  if (value) {
    return moment(String(value)).format('DD/MM/YYYY HH:mm')
  }
});
Vue.filter('formatDate', function(value) {
  if (value) {
    return moment(String(value)).format('DD/MM/YYYY')
  }
});
Vue.filter('formatTime', function(value) {
  if (value) {
    // console.log(value, String(value));
    // console.log(moment.locale());
    return moment(String(value)).format('HH:mm')
  }
});

new Vue({
  el: '#app',
  store, 
  methods: {
    init() {
      moment.locale('en-gb');
      // if (process.env.NODE_ENV === 'development') {
      //   devtools.connect('http://192.168.20.150');
      // }
      this.$store.dispatch('db/populateDB');
      this.$store.dispatch('datafiles/openFS');
      // this.$store.commit('calc/calcSplineCoefficients');
    }
  },
  render: h => h(AppNavigator),
  beforeCreate() {
    this.$ons.ready(() => {
      this.init();
    })
  }
  // beforeDestroy() {
  //   this.$store.commit('db/closeDB');
  // }
});



